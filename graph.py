#
# Plots planck intensity as a function of Temperature for two distincts frequencies
#
import matplotlib.pyplot as plt
import numpy as np
from constants import *
from compute import omega_beam

def planck(T,nu):
    A = 2*hp*nu**3/cc**2
    return A/(np.exp(hp*nu/kb/T)-1)

def bb(l, T):
    A = 2*hp*cc**2/l**5
    return A/(np.exp(hp*cc/kb/T/l)-1)

x_T = np.linspace(1,10,1000)

plt.loglog(x_T,planck(x_T,150e9),label='150GHz')
plt.loglog(x_T,planck(x_T,230e9),label='230GHz')
plt.legend()
plt.show()

fig, axs = plt.subplots(2, 1, constrained_layout=True)
y_150 = planck(x_T,150e9)*1e-6/Jy #MJy/sr
y_230 = planck(x_T,230e9)*1e-6/Jy #MJy/sr

axs[0].loglog(x_T, y_150,label='150GHz')
axs[0].loglog(x_T, y_230,label='230GHz')
axs[0].set_title('MJy/sr')
axs[0].set_xlabel('Temperature (K)')
axs[0].set_ylabel('MJy/sr')
fig.suptitle('Planck intensity', fontsize=16)

# Conversion 1MJy/sr = 3mJy/beam (for a 10'' beam) only for a specific wl !!!
# Indeed we have 10'' ~ 5e-5 rad and Omega = 1.13 theta^2 ~ 1e-9

conv = omega_beam()

axs[1].loglog(x_T, conv[0]*y_150*1e9)
axs[1].loglog(x_T, conv[1]*y_230*1e9)
axs[1].set_xlabel('Temperature (K)')
axs[1].set_title('mJy/beam')
axs[1].set_ylabel('mJy/beam')
