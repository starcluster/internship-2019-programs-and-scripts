##################################################################
# Fitting a blackbody graph on data extracted from fits of L1498 #
##################################################################

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.style as style
from astropy.io import fits
from scipy.ndimage import gaussian_filter

import sys
sys.path.insert(0, '..')
from constants import *

style.use('seaborn-poster') #sets the size of the charts
style.use('ggplot')

def load_fits(name):
    with fits.open(name) as hdu:
        if type(hdu[0].data) is np.ndarray:
            tmp = hdu[0].data[:,0]
            tmp = tmp[0]
            return tmp
        else:
            return hdu[4].data
        
def plot_bb(f250, f350, f500, f1250, f2000):
    # extract flux in MJy/sr from fits file
    # We aree going to consider only 1 pixel first and then we gonna convolue by a 20'' width gaussian
    # f250, f350 and f500 are already in MJy/sr
    ind = np.where(np.isnan(f1250)) # Removes nan values
    f1250[ind] = 0
    # Conversion: mJy/11beam --> MJy/sr
    # first let's compute the beam = 2460/freq(GHz) = 2460*wl/c
    freq = cc/(1250*1e-6)*1e-9 # [GHz]
    print(freq)
    beam = 2460/freq*sec #rad
    omega_beam = 1.133*beam**2
    f1250 = omega_beam*f1250*mJytoMJy # [MJy]
    print(np.max(f1250))
    print(np.max(f2000))
    # and again for 2000µm but this time it is Jy/beam --> MJy/sr
    # todo: factorize this code O_o ! ! !
    f2000 = convert_2mm(f2000)
    print(np.max(f1250))
    # Conversion Jy/10beam --> MJy/sr
    print(np.max(f2000))
    y_values = [np.max(f250), np.max(f350),np.max(f500), np.max(f1250), np.max(f2000)]
    print(y_values)
    x_values = [250, 350, 500, 1250, 2000]
    return (x_values, y_values)

def convert_2mm(f2000):
    freq = cc/(2000*1e-6)*1e-9 # [GHz]
    beam = 2460/freq*sec #rad
    omega_beam = 1.133*beam**2
    f2000 = 1/omega_beam*f2000*1e-6 # [MJy]
    return f2000
    # much more simpler: beam = 1.13 * (17*sec)**2 and then multiply by 1e9
    
def plot_bb_gaussian(names):
    # Extract the flux from a 20'' width gaussian
    datas, pixel_size = [],[]
    for name in names:
        with fits.open(name) as hdu: # We need also the ratio pixel/arcsec
            if type(hdu[0].data) is np.ndarray:
                tmp = hdu[0].data[:,0]
                tmp = tmp[0]
                datas.append(tmp)
                pixel_size.append(hdu[0].header['CDELT1'])
            else:
                datas.append(hdu[4].data)
                pixel_size.append(hdu[4].header['CDELT1'])
    number_pixel = aperture_in_pixel(pixel_size, 20)
    datas[3] = datas[3]*3
    # Bad idea to use the gaussian filter because it reduces data, let's try another way
    convert_2mm_V = np.vectorize(convert_2mm)
    datas[4] = convert_2mm_V(datas[4])
    plt.imshow(datas[4])
    plt.show()
    f2mm = datas[4]
    datas[4] = gaussian_filter(datas[4], 10)
    # datas[4] = datas[4]*circle_mask(datas[4].shape[0]/2,datas[4].shape[1]/2,200, datas[4].shape[0], datas[4].shape[1]) 
    
    print(np.max(datas[4]))
    plt.suptitle("A circle of 20'' width around the maximum pixel value, map2 has been harshly convolued")
    i = 1
    peak_zones = []
    for data,sp in zip(datas,number_pixel):
        ind = np.where(np.isnan(data)) # Removes nan values
        data[ind] = 0
        plt.subplot(2,5,i)
        if i==5 :
            print(type(f2mm))
            mask_applied = apply_mask_2(data, sp)*f2mm
        else:
            mask_applied = apply_mask(data, sp)
        plt.imshow(mask_applied)
        ind = np.nonzero(mask_applied)
        mask_applied = mask_applied[ind]
        peak_zones.append(mask_applied)
        i +=1
    # for data in datas:
    #     plt.subplot(2,5,i)
    #     plt.imshow(data)
    #     i += 1
    for zone in peak_zones:
        plt.subplot(2,5,i)
        plt.hist(zone.ravel(), bins=100, label=str(np.round(np.mean(zone),4))+'\n'+str(np.round(np.std(zone),4)) )
        plt.legend()
        i += 1
        
    
    plt.show()
    return 1,1

def apply_mask(data, diameter):
    x,y = np.unravel_index(data.argmax(), data.shape) # Boring syntax of numpy to get the x,y position and not the indice ...
    print(x)
    mask = circle_mask(x,y,diameter/2,data.shape[0], data.shape[1])
    return mask*data

def apply_mask_2(data, diameter):
    x,y = np.unravel_index(data.argmax(), data.shape) # Boring syntax of numpy to get the x,y position and not the indice ...
    print(x)
    mask = circle_mask(x,y,diameter/2,data.shape[0], data.shape[1])
    return mask

def circle_mask(a,b,r,n,m):
    y,x = np.ogrid[-a:n-a, -b:m-b]
    mask = x*x + y*y <= r*r
    A = np.ones((n, m))
    A[mask] = 0
    return 1-A # returns a matrix with one INSIDE the circle and 0 OUTSIDE
    
def theoretical_bb(T):
    # We compute B_nu 
    x = np.linspace(250*1e-6, 2100*1e-6, 100) # [m]
    A = 2*hp*cc**2/x**5
    y = A/(np.exp(hp*cc/kb/T/x)-1) # [W m-3 sr-1]
    y = x**2/cc*y #[W m-2 Hz-1 sr-1]
    return x,y

if __name__=='__main__':
    x_exp,y_exp = plot_bb(load_fits('../fits/250mic.fits'),
                      load_fits('../fits/350mic.fits'),
                      load_fits('../fits/500mic.fits'),
                      load_fits('../fits/1250mic.fits'),
                      load_fits('../fits/map2.fits'))

    x_exp_g,y_exp_g = plot_bb_gaussian(['../fits/250mic.fits',
                                   '../fits/350mic.fits',
                                   '../fits/500mic.fits',
                                   '../fits/1250mic.fits',
                                    '../fits/map2.fits'])


    x_th, y_th = theoretical_bb(12)
    #print(y_th)
    plt.plot(np.array(x_exp)*1e-6, y_exp, 'o')
    plt.loglog(x_th, y_th*1e20)
    plt.xlabel("Wavelength (m)")
    plt.ylabel("MJy/sr")
    plt.show()
    # take 0.7 for the last point
