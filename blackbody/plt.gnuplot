set terminal wxt size 1200,800 enhanced font 'Verdana,20' persist
# set terminal latex
set border linewidth 1.5
set style line 1 linecolor rgb '#FF0000' linetype 1 linewidth 2
set style line 2 linecolor rgb '#000080' linetype 1 linewidth 2
set samples 10000
set title "Intensity"
set xlabel "Wavelength [m]"
set ylabel "Intensity [MJy/sr]"
set logscale
set terminal pngcairo size 1200, 800  enhanced font 'Verdana,30'
set grid 
hp = 6.626e-34
cc = 3e8
kb = 1.38e-23
mH = 1.6e-27
T = 9
NH = 0.79e26
beta = 2
kappa_250 = 0.009
kappa_300 = 0.1
bb(x) = 2*hp*cc/x**3/(exp(hp*cc/kb/T/x)-1)*1e20
o(x) =  1 - exp(-1.4*mH*NH*kappa_300*(x/(300e-6))**-beta)
gb(x) = o(x)*bb(x)
# Plot
fit gb(x) "intensity3.dat" u 1:2:3 yerr via NH, T,beta
plot [80e-6:2100e-6] gb(x) title 'graybody' with lines linestyle 2 lt 1 lw 8,\
                     "intensity3.dat" u 1:2:3 title 'l1498'  with errorbars linestyle 7 ps 5 pt 11

