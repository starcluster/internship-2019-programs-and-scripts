# In this file, I added data from the planck catalog

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.style as style
from graybody import graybody_V

style.use('seaborn-poster') #sets the size of the charts
style.use('ggplot')

hp = 6.626e-34
kb = 1.38e-23
cc = 3e8
sec = 1/206265
mJytoMJy = 1e9
kernelG = 3


if __name__=='__main__':
    # According to planck catalog at wl = 350µ, we have a flux of 81 Jy/N beam
    # As we have 110.9 with IRAM
    # Let's say that we have factor q = 1.37
    q = 1.37
    l_exp = np.array([99,250,350,500,550,850,1250,2000])*1e-6
    # Possible mistake with units --->
    y_exp = np.array([1.45, 133.95226, 110.985115, 58.66065, 32, 9.6, 1.7, 0.7])
    y_exp_hyp = np.array([1.45*q, 133.95226, 110.985115, 58.66065, 32*q, 9.6*q, 5.5, 0.7])
    print(y_exp_hyp)
    print(l_exp)
    plt.subplot(121)
    plt.plot(l_exp, y_exp_hyp, '.')
    l = np.linspace(50e-6, 5000e-6, 1000)
    T = 12.6
    beta = 2
    y = graybody_V(l, T, beta)*1e20
    plt.suptitle(r"$T$=%.1f  $\beta$=%.1f"%(T, beta))
    plt.plot(l,y)
    plt.subplot(122)
    plt.plot(l_exp, y_exp_hyp, '.')
    plt.loglog(l,y)
    plt.show()
    # #1
    sigma = np.array([0.98, 0.7, 0.43, 0.3, 0.4])
    
# lire physical parameters planck (6)
# tracer la source cf données catalogue
# recalculer la densté de colonne
# sigma^2 = sigmacal^2 + sigmadispersion^2
# regarder à 1mm
