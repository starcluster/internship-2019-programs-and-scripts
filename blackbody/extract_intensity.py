######################################
# Extracting intensity from the peak #
######################################

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.style as style
from astropy.io import fits
from scipy.ndimage import gaussian_filter

import sys
sys.path.insert(0, '..')
from constants import *
from blackbody import *

style.use('seaborn-poster') #sets the size of the charts
style.use('ggplot')

def load_fits(name):
    with fits.open(name) as hdu:
        return hdu[0].data, hdu[0].header['CDELT1']

def locate_max(data):
    return np.unravel_index(data.argmax(), data.shape)

def extract_intensity(datas):
    # xmax, ymax = locate_max(datas[3])
    # print(xmax, ymax)
    uo = open("l1521e.dat", "w")
    wl = ['1300e-6', '250e-6', '350e-6', '500e-6']
    i = 0
    for data in datas:
        n,m = data.shape
        xmax, ymax = locate_max(data)
        mask = circle_mask(xmax, ymax, 20/2.6, n, m) #1 pixel is 2.6''
        masked = data*mask
        ind = (masked != 0)
        avg, std = np.average(masked[ind]), np.std(masked[ind])
        major = np.max(data)
        print("%.1f %.1f"%(major, std))
        uo.write("%s %.1f %.1f\n"%(wl[i], avg, std))
        i += 1
        plt.imshow(data)
        plt.colorbar()
        plt.show()
    uo.close()
if __name__ == '__main__':
    datas = []
    """
    names = ['../fits/250mic.fits',
             '../fits/350mic.fits',
             '../fit/500mic.fits',
             '../fits/1250mic.fits',
             '../fits/1mm_12.fits',
             '../fits/2mm_12.fits']
    """
    names = ['../fits_l1521e/l1521e_13mm.fits',
             '../fits_l1521e/l1521e_250mic.fits',
             '../fits_l1521e/l1521e_350mic.fits',
             '../fits_l1521e/l1521e_500mic.fits']
    
    for name in names:
        data, delta = load_fits(name)
        print(1/delta*35/3600 )
        data = gaussian_filter(data, -1/delta*0.00972222)
        datas.append(data)

    # beam = 1.13*(sec*12.5)**2
    beam = 1.13*(sec*11)**2
    # datas[0] *= 1/beam*1e-6
    datas[0] *= 1/3.2

    
    for data in datas:
        ind = np.where(np.isnan(data))
        data[ind] = 0                               
    extract_intensity(datas)

    
