#################################################################
# Fitting a graybody graph on data extracted from fits of L1498 #
#################################################################

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.style as style

import sys
sys.path.insert(0, '..')
from constants import *

kappa_250 = 0.009 # [m2 kg-1]
beta = 1.6
NH = 2.2e26 # [m-2]

style.use('seaborn-poster') #sets the size of the charts
style.use('ggplot')

def blackbody(l, T=10):
    # We compute B_nu
    A = 2*hp*cc**2/l**5
    y = A/(np.exp(hp*cc/kb/T/l)-1) # [W m-3 sr-1]
    y = l**2/cc*y # [W m-2 Hz-1 sr-1]
    return y

blackbody_V = np.vectorize(blackbody)

def opacity(l, beta):
    kappa = kappa_250*(l/(250e-6))**(-beta) # [m2 kg-1]
    tau = NH*kappa*mH*1.4
    return (1-np.exp(-tau))

def graybody(l, T=10, beta=2):
    return blackbody(l, T)*opacity(l, beta)

graybody_V = np.vectorize(graybody)

def rayleigh_jeans(l, T):
    return (2*kb*T/l**2) 

rayleigh_jeans_V = np.vectorize(rayleigh_jeans)

def rayleigh_jeans_gray(l, T, beta):    
    return rayleigh_jeans(l, T)*opacity(l, beta)

rayleigh_jeans_gray_V = np.vectorize(rayleigh_jeans_gray)

l = np.linspace(50e-6, 5000e-6, 1000)
for T in [10]:
    y = graybody_V(l, T, beta)    
    plt.plot(l, y*1e20, label=r'$T$ = ' + str(T) + ' K')
    #plt.plot(l, rayleigh_jeans_V(l,T)*1e20, label="RJ without opacity")
    plt.plot(l, rayleigh_jeans_gray_V(l,T, beta)*1e20, label="RJ with opacity")

    plt.legend()
l_exp = np.array([250,350,500,550,1250,2000])*1e-6
y_exp = np.array([133.95226, 110.985115, 58.66065,32, 1.7, 0.7])
sigma = np.array([0.98, 0.7, 0.43,0, 0.3, 0.4])


#plt.xlim(1e-4, 1e-3)
plt.errorbar(l_exp, y_exp, fmt='.',  yerr=sigma, label='exp')
plt.legend()
print("question", graybody(2000*1e-6, 10, 1.6)*1e20)
plt.xscale('log', nonposx='clip')
plt.yscale('log', nonposy='clip')
plt.xlabel('m')
plt.ylabel('MJy/sr')
# plt.plot(l, blackbody(l)*1e20)
plt.title(r"Graybody fitting at $\beta$ = " + str(beta) + ' $N_H=$ ' + str(NH))
plt.show()

