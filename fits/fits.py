#!/usr/bin/python3

#################################################
######## Open and display FITS file #############
#################################################

import argparse
import matplotlib.pyplot as plt
import numpy as np
from astropy.io import fits
from datetime import datetime, timedelta
from astropy.wcs import WCS

# PARSING
parser = argparse.ArgumentParser()
parser.add_argument("mode", help="Choose the mode: open or edit")
parser.add_argument("file", help="Name of the file")
parser.add_argument("-v", "--verbose", help="Display more information about the file", action="store_true")
parser.add_argument("-s", "--save", help="Save the file instead of displaying it")
parser.add_argument("--inferno", help="Enable inferno mode, useful for colorblind people.",  action="store_true")
parser.add_argument("-c","--contrast", help="Increase contrast when the value is very small")
parser.add_argument("-g","--grids", help="Display the grid, coordinate system has to be specified: fk4, fk5, galactic or supergalactic")
args = parser.parse_args()

# FUNCTIONS
def open_fits(path_to_the_file, verbose):
    #
    # The main function of the open mode: display or save image according to the args
    #
    headers = [] # headers will be a list of dicts
    datas = [] # datas will be a list of numpy array
    
    # Opening the file, extracting the datas and closing the file:
    with fits.open(path_to_the_file) as hdul:
        nf = 0 # let's keep in memory the number of fits image that we have extracted
        hdul.info()
        a = input()
        for i in range(len(hdul)):
            if type(hdul[i].data) is np.ndarray:
                if verbose:print(repr(hdul[i].header))
                headers.append(hdul[i].header)
                datas.append(hdul[i].data)
                nf += 1 # useful ?
        #header = hdul[0].header
        #data = hdul[0].data
    print(nf)
    return (headers, datas)

def display(headers, datas, output, verbose, inferno, contrast, grids):
    for header, data in zip(headers, datas):
        if header["NAXIS"] == 4:
            data = data[:,0]
            data = data[0]
            
        plt.figure()
        
        ax = setting_axes(header, data)

        #Displaying Image 
        if inferno: cmap_value = "inferno"
        else: cmap_value = "gray"
        print(cmap_value)
        if contrast != None:
            im = ax.imshow(data, cmap=cmap_value, origin='lower', clim=(0.0, float(contrast)))
        else:
            ax.imshow(data, cmap=cmap_value, origin='lower')
        #ax.imshow(data, origin='lower', clim=(0.0, 0.005))
        
        #GRIDS
        if grids != None:
            ax.coords.grid(True, color='white', ls='solid')
            ax.coords[0].set_axislabel('Galactic Longitude')
            ax.coords[1].set_axislabel('Galactic Latitude')
        
            overlay = ax.get_coords_overlay(grids)
            overlay.grid(color='white', ls='dotted')
            overlay[0].set_axislabel('Right Ascension ' + grids)
            overlay[1].set_axislabel('Declination ' + grids)
            #overlay[0].set_axislabel('Right Ascension (J2000)')
            #overlay[1].set_axislabel('Declination (J2000)')

        #im = 
        if output == None:
            plt.title(header['EXTNAME'])
            plt.show()
        else: plt.savefig(output)
        
        if verbose:print(data)

def degrees_to_hours(degrees):
    #useless ? 
    seconds = degrees*24*3600/360 #degrees -> seconds
    sec = timedelta(seconds)
    d = datetime(1,1,1)
    return (d.hour, d.minute, d.second)

def setting_axes(header, data):
    # Configure the axes to use world coordinate system
    
    wcs = WCS(header)
    wcs = wcs.celestial
    #try:
    ax = plt.subplot(projection=wcs)
    #except IndexError:
     #   print("An error has occured, projection on wcs was not possible")
      #  ax = plt.subplot()
    return ax
    
if args.mode == "open":
    headers, datas = open_fits(args.file, args.verbose)
    display(headers, datas, args.save, args.verbose, args.inferno, args.contrast, args.grids)

elif args.mode == "edit":
    print("This option has not been implemented yet")

elif args.mode == "averaging":
    print("wip")
    
else:
    print("Error: Unknown Option")


#TODO verbose, axes, etc ...
#plt imshow origin = lower (python for astronomers) handling fits file: ok
#celestial coordinate :ok
#astronomical coordinate system (astropy docs): ok
#makeing plot with world cooredinate and right ascension :ok
#relative coordinate :ok
#Je suis dans coloraar
#là j'y suis 
