##################################################################################
# Correcting errors (negative values) induced by data processing in average.fits #
# So far: mainly analyzing different runs                                        #
##################################################################################

import scipy.stats
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.style as style
from astropy.convolution import convolve, Gaussian1DKernel, Box1DKernel
from astropy.io import fits
from scipy.ndimage import gaussian_filter

style.use('seaborn-poster') #sets the size of the charts
style.use('ggplot')

wl = '2mm'

kernelG11 = 12.5
kernelG17 = 18.5

def load_fits(name):
    with fits.open(name) as hdu:
        if type(hdu[0].data) is np.ndarray:
            tmp = hdu[0].data[:,0]
            tmp = tmp[0]
            return tmp
        else:
            return hdu[4].data

def slice(sx, name1, name2):
    data1 = load_fits(name1)
    data2 = load_fits(name2)
    # Display image convoluted by gaussian
    plt.subplot(2, 2, 1)
    plt.title(name1 + " not convolued")
    i_min = 100
    i_max = 300
    kernel = 4
    data1s = gaussian_filter(data1,kernel)
    print(np.sum(data1)) 
    print(np.sum(data1s))
    # Checking if the gaussian is normalized (we have to maintain the flux identical
    data_crop1 = data1s[i_min:i_max,i_min:i_max]
    plt.imshow(data1, vmin=-0.01, vmax=0.01)
    plt.colorbar()
    plt.subplot(2, 2, 2)
    plt.title(name2 + " not convolued")
    data2s = gaussian_filter(data2,kernel)
    data_crop2 = data2s[i_min:i_max,i_min:i_max]
    plt.imshow(data2,vmin=-0.01, vmax=0.01)
    plt.colorbar()
    plt.show()

    # Display histogram of sub for all the images (no masking but cropping)
    y1 = data_crop1.ravel() #data1[sx:sx+1,150:350].ravel()
    y2 = data_crop2.ravel() #[sx:sx+1,150:350].ravel()
    sub = y1 - y2
    print(np.mean(sub), np.std(sub))
    plt.subplot(2,2,3)
    plt.title(name1 + ' - ' + name2 + ' convolued and cropped ' + str(i_min) + ' ' +  str(i_max))
    plt.hist(sub, bins=100, label=str(np.round(np.mean(sub),4)) + '\n' + str(np.round(np.std(sub),4)))
    plt.legend()
    plt.xticks(rotation=90)
    plt.subplot(2,2,4)
    plt.title("Linear regression on scatter plot convolued and cropped")
    plt.xlabel(name1)
    plt.ylabel(name2)
    plt.plot(y1, y2, '.')
    slope, vertical_intercept, corr, pvalue, err = np.round(scipy.stats.linregress(y1, y2), 3)
    x_line = np.linspace(-0.001,0.005, 100)
    line = slope*x_line + vertical_intercept
    plt.plot(x_line,line, linestyle ='--', label = 'm=' + str(slope) + '\np=' + str(vertical_intercept) + '\nr=' + str(corr))
    plt.legend()
    plt.show()

    # Display for a slice 
    x = np.linspace(0,249, 200)
    y1 = data1[sx:sx+1,150:350].ravel()
    size_kernel = 20
    gauss_kernel = Gaussian1DKernel(size_kernel)
    y1s = convolve(y1, gauss_kernel)
    y2s = convolve(y2, gauss_kernel)
    subs = convolve(sub, gauss_kernel)
    plt.plot(x, y1, label="map1.fits")
    #plt.plot(x, y2s, label="map3.fits")
    #plt.plot(x, subs, label="sub")
    plt.legend()
    plt.title("Slice at x = " + str(sx) + "--2mm -- Size Kernel= " + str(size_kernel))
    #plt.savefig(str(sx) + '.png')
    #plt.clf()
    plt.show()
    # slope, vertical_intercept, corr, pvalue, err = np.round(scipy.stats.linregress(y1, y2), 3)
    # x_line = np.linspace(-0.001,0.005, 100)
    # line = slope*x_line + vertical_intercept

    
    z_min = -1e-2
    z_max = 1e-2
    plt.plot(y1, y2, '.')
    plt.xlim(z_min,z_max)
    plt.ylim(z_min,z_max)
    plt.plot(x_line,line, color = 'green', linestyle ='--', label = 'm=' + str(slope) + '\np=' + str(vertical_intercept) + '\nr=' + str(corr)) 
    plt.legend()
    plt.title("Scatter plot map1.fits / map3.fits -- 2mm")
    #plt.savefig(str(sx)+ '_scatter' + '.png')
    #plt.clf()
    plt.show()
    
def compare(fs):#f250, f350, f500, f1250, f2000):
    wls = ['250µ', '350µ', '500µ', '1.25mm', '2mm (map2)']
    i = 1
    for fit,name in zip(fs, wls):
        plt.subplot(2,3,i)
        i += 1
        if name == '2mm (map2)':
            plt.imshow(fit,vmin=-0.01, vmax=0.01)
            plt.contour(gaussian_filter(fit,10), colors='white', alpha=0.5)
        else:
            plt.imshow(fit)
            plt.contour(fit,  colors='white', alpha=0.5)
        #plt.colorbar()
        plt.title(name)
    plt.show()
    # Rewriting of slice to focus on the total image
    
def compare_runs(fs):
    run1, run2, run3 = fs[0], fs[1], fs[2]
    run1G, run2G, run3G = gaussian_filter(run1, kernelG17), gaussian_filter(run2, kernelG17), gaussian_filter(run3, kernelG17)
    sub1s = run1G - run2G
    sub2s = run1G - run3G
    sub3s = run2G - run3G
    j = 1
    for i,name,k in zip([sub1s, sub2s, sub3s], ['1 sub 2', '1 sub 3', '2 sub 3'], [run1G, run2G, run3G]):
        plt.subplot(2,3,j)
        plt.imshow(i)
        plt.colorbar()
        plt.title("%s peak value =%.4f sigma = %.4f"%(name,np.max(i), np.std(i)))
        plt.subplot(2,3,j+3)
        plt.imshow(k)
        plt.colorbar()
        plt.title("run %d peak value =%.4f"%(j,np.max(k)))
        j += 1
def data_scatter(run1, run2):
    pass

    plt.show()
# slice(230, "../fits/map1.fits","../fits/map2.fits")
compare_runs([load_fits("../fits/map1.fits"), load_fits("../fits/map2.fits"), load_fits("../fits/map3.fits")])
data_scatter(load_fits("../fits/map1.fits"), load_fits("../fits/map1.fits"))
