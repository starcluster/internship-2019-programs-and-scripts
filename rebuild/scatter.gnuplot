set terminal wxt size 800,800 persist
# set terminal latex
set border linewidth 1.5
set style line 1 linecolor rgb '#FF0000' linetype 1 linewidth 2
set style line 2 linecolor rgb '#000080' linetype 1 linewidth 2
set samples 10000
set title "Scatter plot run1/run2 center of source"
set xlabel "run1"
set ylabel "run2"
set xrange [-0.1:0.1]
set yrange [-0.1:0.1]
set terminal pngcairo size 800, 800
plot "scatter.dat"
