#####################################################
# Compute RMS based on a mask of the original image #
#####################################################

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
from astropy.io import fits

wl = '1mm'

def compute_rms(av, mask, nhits):
    masked = av*mask*np.sqrt(nhits)
    ind = (masked != 0)
    histo = masked[ind].ravel() # flatten to be used as histograms values
    avg, var = np.mean(histo), np.var(histo)
    pdf_x = np.linspace(np.min(histo), np.max(histo), 1000)
    print(np.std(histo))
    pdf_y = np.exp(-0.5*(pdf_x-avg)**2/var)*1/np.sqrt(2*np.pi*var)
    plt.hist(masked[ind].ravel(), density = True, bins = 100, log = False)
    #plt.xlim(-0.04,0.04)
    plt.plot(pdf_x, pdf_y)
    plt.title(r'$\mathrm{Histogram\ of\ Noise\ %s:}\ \mu=%.4f,\ \sigma=%.4f$' %(wl,avg, np.sqrt(var)))
    plt.show()

def load_mask():
    mask = mpimg.imread("mask.png")
    mask = np.where(mask != 0, 1, 0)
    # cleaning because some values are close to 1 but not exactly 1
    return mask


def load_fits():
    with fits.open("average_" + wl + ".fits") as hdu:
        return hdu[1].data, hdu[4].data
    
def load_fits_2(name):
    with fits.open(name) as hdu:
        return hdu[4].data

def data_scatter(run1, run2):
    run1 *= load_mask()
    run2 *= load_mask()
    plt.imshow(run1)
    plt.show()
    ind = (run1 != 0)
    uo = open("scatter.dat", 'w')
    ll = [i for i in range(run1.shape[1])]
    for i in ll:
        for j in ll:
            print(i,j)
            uo.write("%.4f %.4f\n"%(run1[i,j], run2[i,j]))
    uo.close()

data_scatter(load_fits_2('../fits/map1.fits'),load_fits_2( '../fits/map2.fits'))
av, nhits = load_fits()
compute_rms(av, load_mask(), nhits)

#12.5 '' 1mm fwhm
#18.5 '' 2mm fwhm
#/!\ Jy/beam -> MJy/sr x1.3
