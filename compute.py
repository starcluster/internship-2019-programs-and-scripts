########################################
# Useful computation for other scripts #
########################################

import numpy as np
from constants import sec

def omega_beam():
    nu1 = 150 #GHz
    nu2 = 230 #GHz
    freq = np.array([150.,230.])
    beam = 2460./freq  # [arcsec]
    print(sec)
    Omega_beam = 1.133*(beam*sec)**2
    print(nu1, ": Ω_beam=", Omega_beam[0])
    print(nu2, ": Ω_beam=", Omega_beam[1])
    return (Omega_beam[0], Omega_beam[1])
