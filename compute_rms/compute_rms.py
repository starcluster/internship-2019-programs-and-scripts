#####################################################
# Compute RMS based on a mask of the original image #
#####################################################

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
from astropy.io import fits

wl = '2mm'

def compute_rms(av, mask, nhits):
    masked = av*mask*np.sqrt(nhits)
    ind = (masked != 0)
    histo = masked[ind].ravel() # flatten to be used as histograms values
    avg, var = np.mean(histo), np.var(histo)
    pdf_x = np.linspace(np.min(histo), np.max(histo), 1000)
    print(np.std(histo))
    pdf_y = np.exp(-0.5*(pdf_x-avg)**2/var)*1/np.sqrt(2*np.pi*var)
    plt.hist(masked[ind].ravel(), density = True, bins = 100, log = False)
    #plt.xlim(-0.04,0.04)
    plt.plot(pdf_x, pdf_y)
    plt.title(r'$\mathrm{Histogram\ of\ Noise\ %s:}\ \mu=%.4f,\ \sigma=%.4f$' %(wl,avg, np.sqrt(var)))
    plt.show()

def compute_rms_all_run(r1, r2, r3, nh1, nh2, nh3, mask):
    r1, r2, r3 = r1*mask*np.sqrt(nh1),r2*mask*np.sqrt(nh2),r3*mask*np.sqrt(nh3)
    ind1, ind2, ind3 = (r1 != 0), (r2 != 0), (r3 != 0)
    r1, r2, r3 = r1[ind1], r2[ind2], r3[ind3]
    i = 1
    for r,name in zip([r1, r2, r3], ['Brightness 1mm run1', 'Brightness 1mm run2', 'Brightness 1mm run3']):
        plt.subplot(1,3,i)
        plt.ylim(0,4)
        plt.xlim(-0.5, 0.5)
        avg, var = np.mean(r.ravel()), np.var(r.ravel())
        pdf_x = np.linspace(np.min(r), np.max(r), 300)
        pdf_y = np.exp(-0.5*(pdf_x-avg)**2/var)*1/np.sqrt(2*np.pi*var)#*np.sum(r)
        plt.hist(r.ravel(), bins = 100,density=True,  label = r'$\mu=%.4f,\ \sigma=%.4f$' %(avg, np.sqrt(var)))
        pix = 0.000555*3600  # ''arcsec^2
        sigma_sample = np.sqrt(var)
        Nhits = nh1[230,215]
        N_pix_per_beam  = 2*np.pi*(18.5/2.35/pix)**2
        sigma_beam = sigma_sample/np.sqrt(Nhits)
        sigma_pixel = sigma_beam/np.sqrt(N_pix_per_beam)
        print("sigma_sample: %s\nNhits: %s\n N_pix_per_beam: %s\n sigma_beam: %s\n sigma_pixel: %s\n"%(sigma_sample, Nhits, N_pix_per_beam, sigma_beam, sigma_pixel))
        exit()
        print(np.sqrt(var)/np.sqrt(nh1[230,215]*2*np.pi*(18.5/2.35)**2/pix**2))
        plt.plot(pdf_x, pdf_y, color='red')
        plt.legend()
        plt.title('Histogram of Noise for ' + name)
        i += 1
    plt.show()
    #  Bonus with transparency:
    kwargs = dict(histtype='stepfilled', alpha=0.3, density=True, bins=100)
    for r,name in zip([r1, r2, r3], ['map1.fits', 'map2.fits', 'map3.fits']):
        plt.hist(r.ravel(), **kwargs, label=name)
        avg, var = np.mean(r.ravel()), np.var(r.ravel())
        pdf_x = np.linspace(np.min(r), np.max(r), 300)
        pdf_y = np.exp(-0.5*(pdf_x-avg)**2/var)*1/np.sqrt(2*np.pi*var)
        plt.plot(pdf_x, pdf_y, label= 'Gaussian of ' + name)
        plt.legend()
    plt.show()
    
def load_mask():
    mask = mpimg.imread("mask.png")
    mask = np.where(mask != 0, 1, 0)
    # cleaning because some values are close to 1 but not exactly 1
    return mask

def load_fits(name):
    # /!\ Be extremely careful here, hdu[4] can be sometimes nhits or 2mm ! 
    #with fits.open("average_" + wl + ".fits") as hdu:
    with fits.open(name) as hdu:
        return hdu[4].data, hdu[6].data

# av, nhits = load_fits()
# compute_rms(av, load_mask(), nhits)

r1, nh1 = load_fits("../fits/map1.fits")
r2, nh2 = load_fits("../fits/map2.fits")
r3, nh3 = load_fits("../fits/map3.fits")
compute_rms_all_run(r1, r2, r3, nh1, nh2, nh3, load_mask())

#12.5 '' 1mm fwhm
#18.5 '' 2mm fwhm
#/!\ Jy/beam -> MJy/sr x1.3
