#############################################
# Most used constants and conversions rules #
#############################################

import numpy as np

hp = 6.626e-34 # [m2 kg s-1 ]
cc = 3e8 # [m s-1]
kb = 1.38e-23 # [m2 kg s-2 K-1]
mH = 1.6e-27 # [kg]
Jy = 1e-26  # [W m-2 Hz-1]
ghz2K = hp/kb # [GHz K-1] From GHz to Kelvin
K2ghz = kb/hp # [K GHZ-1] From Kelvin to Ghz
MJysr2mJybeam = 0.75e6
mJybeam2MJysr = 1.33e-6
mJytoMJy = 1e9
sec = 1./206265 # 1 arcsec in rad


def aperture_in_pixel(pixel_size, size_aperture):
    # size_aperture has to be expressed in arcsec
    # According to the header, pixel_size is in degree so multiplying by 3600 will give us arcsec
    pixel_size_arcsec = np.array(pixel_size)*3600
    # As we want a size_aperture'' aperture, we just divide size_aperture by              pixel_size_arcsec
    number_pixel = np.round(size_aperture/pixel_size_arcsec)
    # Means we are gonna explore a number_pixel size region around the peak
    return number_pixel

def pixel_in_aperture_arcsec(nbpix, size_aperture):
    return size_aperture/nbpix
