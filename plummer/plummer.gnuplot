set terminal wxt size 1200,800 enhanced font 'Verdana,10' persist
# set terminal latex
set border linewidth 1.5
set style line 1 linecolor rgb '#FF0000' linetype 1 linewidth 2
set style line 2 linecolor rgb '#000080' linetype 1 linewidth 2
set samples 10000
set title "Intensity"
set xlabel "Wavelength [m]"
set ylabel "Intensity [MJy/sr]"
set logscale
# set terminal pngcairo size 1200, 800
p = 4
n(x) = 1/(1+x**p)
np(x) = 1/(1+x**2)**(p/2)

serie(x) = 1 - (p-1)*x**2 - 1/6*(3*p**2 - 5*p +2)*x**4 - 1/90*(15*p**3-30*p**2+19*p-4)*x**6
plot n(x), np(x), serie(x)
