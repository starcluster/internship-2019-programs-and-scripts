#+TITLE: README <2019-07-25 jeu.>
* averaging 
Les fonctions suivantes peuvent être utilisées:
averaging12: effectue la moyenne pondéré de deux fichiers fits:
#+BEGIN_SRC python
averaging12(hdu[0].data, hdu[3].data, hdu[1].data, hdu[4].data, hdu[2].data, hdu[5].data)
#+END_SRC
dépend de compute_weights
write_fits permet d'écrire dans un fits un fichier moyenné avec stddev, le mask et nhits
* blackbody
** blackbody.py:
 Ce script charge les fichiers fits situé dans le répertoire ../fits,
effectue les bonnes conversions et applique un masque circulaire
autour du pic pour déterminer une intensité avec une incertude. Le
résultat est tracé à l'aide de matplotlib.
** graybody.py:
Fonctions utilisables:
blackbody(l,T): intensité d'un bb à longueur d'onde l et température T en [W m-2 Hz-1 sr-1 ]
opacity(l, beta):
graybody(l, T, beta): intensité corps gris en [W m-2 Hz-1 sr-1 ]
** plt.gnuplot
Fit le model de corps gris sur les donnes
* compute_rms
Calcul le bruit moyen de tout les runs, lancer avec python3 compute_rms.py
* daily
Tous les rapports et compte rendu qui étaient hébergés sur ma page web
* final_report
Le rapport final sans les .png (le git ignore ne suit pas les .png)
* intensity 
Plusieurs scripts pour calculer le profil d'intensités du coeur pré-stellaire:
intensity_basic.py: utilise des anneaux concentriques, les résultats son stockés dans rprofile.dat
intensity_elipses.py: meme chose mais avec des elipses(rmin, rmax, theta)
intensity_tracks.py: intensités le long d'une ligne
intensity_normalized.gnuplot: fitter le profil d'intensités
* maps
Divers résultats en png
* plummer
Tentatives d'utiliser les fonctions de plummer, pas de primitives
simples pour alpha quelconque donc abandon.
* rebuild
scatter.gnuplot: Tracer des scatter plots entre les fits
rebuild.py: comparer les fits en les soustrayant
* report 
Rapport préliminaire à mi-stage
* report_bilal
rapport suite à des anomalies dans les fits
* slides
les slides de trois présentations que j'ai fait pendant les réunions spectres
* vizier
données du catalogue CDS
