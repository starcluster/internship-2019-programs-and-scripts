########################
# Drawing Map with ISO #
########################

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.style as style
from astropy.io import fits
from astropy.wcs import WCS
from astropy.utils.data import get_pkg_data_filename
from scipy.ndimage import gaussian_filter

import sys
sys.path.insert(0, '..')
from constants import *

style.use('seaborn-poster') #sets the size of the charts
style.use('ggplot')

def load_fits_n(name, n):
    with fits.open(name) as hdu:
        return hdu[n].data 
    
def maps(lmaps, names):
    for i in range(len(lmaps)):
        # filename = get_pkg_data_filename(names[i])
        hdu = fits.open(names[i])[0]
        wcs = WCS(hdu.header)
        wcs = wcs.celestial
        ax = plt.subplot(projection=wcs)
        overlay = ax.get_coords_overlay("galactic")
        overlay.grid(color='white', ls='dotted')
        plt.imshow(lmaps[i], origin='lower')
        plt.grid(color='white', ls='solid')
        plt.title(names[i][8:])
        # plt.colorbar()
        plt.contour(lmaps[i], colors='white', alpha=0.5)
        plt.savefig(names[i][8:] + ".png")
        plt.show()
    
if __name__=='__main__':
    
    names = ['../fits/250mic.fits',
             '../fits/350mic.fits',
             '../fits/500mic.fits',
             '../fits/1250mic.fits',
             '../fits/1mm_12.fits',
             '../fits/2mm_12.fits']
    lmaps = []
    for name in names:
        lmaps.append(load_fits_n(name, 0))
    # lmaps.append(load_fits_n(names[-1], 1))
    beam = 1.13*(12.5*sec)**2
    lmaps[-2] *= 1/beam*1e-6
    for k in range(len(lmaps)):
        lmaps[k] = gaussian_filter(lmaps[k], 6)
    maps(lmaps, names)

    

