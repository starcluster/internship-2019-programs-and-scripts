##########################################
# Averaging images using stds as weights #
##########################################

import time
import numpy as np
import matplotlib.pyplot as plt
from astropy.io import fits
from scipy.ndimage import gaussian_filter

import sys
sys.path.insert(0, '..')
from constants import *

wl = '2mm'

def open_fits():
    datas, stds, nhits = [], [], []
    for path_to_the_file in ['../fits/map1.fits', '../fits/map2.fits', '../fits/map3.fits']:
        with fits.open(path_to_the_file) as hdul:
            for i in range(len(hdul)):
                if type(hdul[i].data) is np.ndarray:
                    if hdul[i].header['EXTNAME'] == 'Brightness_' + wl:
                        hd = hdul[i].header # saved for later
                        datas.append(hdul[i].data)
                        print("ok")
                    elif hdul[i].header['EXTNAME'] == 'Stddev_' + wl:
                        stds.append(hdul[i].data)
                    elif hdul[i].header['EXTNAME'] == 'Nhits_' + wl:
                        nhits.append(hdul[i].data)
                    else:
                        pass
    return datas, stds, hd, nhits

def compute_weights(std):
    weights = np.zeros_like(std)
    ind = (std != 0)
    weights[ind] = 1.0/std[ind]**2
    return weights

def averaging(datas, stds, nhits):
    av = np.zeros_like(datas[0])
    norm = np.zeros_like(av)
    m2d  = np.ones_like(av)
    for data, std in zip(datas, stds):
        w     = compute_weights(std)
        av   += data*w
        norm += w
        m2d   = np.where(w == 0, 0, m2d)
    print(len(m2d[(m2d== 0)].ravel()))
    av = np.where(norm != 0, av/norm, 0)*m2d
    nhits_total = nhits[0] + nhits[1] + nhits[2]
    return norm, m2d, av, nhits_total

def averaging12(run1, run2, std1, std2, nhits1, nhits2):
    av = np.zeros_like(datas[0])
    norm = np.zeros_like(av)
    m2d  = np.ones_like(av)
    for data, std in zip([run1, run2], [std1, std2]):
        print(data[215,215])
        w     = compute_weights(std)
        print(w[215,215])
        av   += data*w
        norm += w
        m2d   = np.where(w == 0, 0, m2d)
    # print(len(m2d[(m2d== 0)].ravel()))
    beam = 1.13*(18.5*sec)**2
    av = np.where(norm != 0, av/norm, 0)*m2d
    av *=1/beam*1e-6
    run1 *=1/beam*1e-6
    run2 *=1/beam*1e-6
    print(run1[230,215])
    print(run2[230,215])
    print(av[230,215])
    # print(norm[215:230,215:230])
    # print(np.max(av))
    plt.imshow(gaussian_filter(av, 6))
    plt.colorbar()
    hdu = fits.open("../fits/map1.fits")
    hdu[1].data = av
    hdu[1].header['EXTNAME'] = 'run12_Brightness_average_2mm_adapted' ## I need stdhits adapted
    for i in range(18):
        hdu.pop()
    hdu.writeto('run12.fits')
    plt.show()
    


def write_fits(hd, norm, m2d, av, nhits_total):
    # hdu = fits.PrimaryHDU(n)
    # hdu.data = 
    hdu = fits.open("../fits/map1.fits")
    hdu[1].data = av
    hdu[1].header['EXTNAME'] = 'Brightness_average_' + wl
    hdu[2].data = norm
    hdu[2].header['EXTNAME'] = 'Stddev_average_' + wl
    hdu[3].data = m2d
    hdu[3].header['EXTNAME'] = 'Mask_average_' + wl
    hdu[4].data = nhits_total
    hdu[4].header['EXTNAME'] = 'Nhits_total_' + wl
    for i in range(15):
        hdu.pop()
    # hdu.writeto('average_' + wl + '.fits')

def crop_center(img,cropx,cropy):
    y,x = img.shape
    startx = x//2-(cropx//2)
    starty = y//2-(cropy//2)    
    return img[starty:starty+cropy,startx:startx+cropx]

def compare_fits(datas):
    # We are going to compare only the central region
    run1 = crop_center(datas[0], 250, 250)
    run2 = crop_center(datas[1], 250, 250)
    run3 = crop_center(datas[2], 250, 250)
    sub1 = np.abs(run1 - run2)
    sub2 = np.abs(run1 - run3)
    sub3 = np.abs(run2 - run3)
    plt.imshow(sub1)
    plt.show()
    
    print("1->",np.mean(run1),"2->", np.mean(run2), "1 - 2-> ", np.mean(sub1))
    print("2->",np.max(run2),"3->", np.max(datas[2]), "2 - 3-> ", np.max(sub2))
    print("1->",np.max(run3),"3->", np.max(datas[2]), "1 - 3-> ", np.max(sub3))

def renormalized():
    hdu = fits.open("../fits/500mic.fits")
    # hdu[0].data = hdu[0].data*0.9/58.6
    hdu[0].data = hdu[0].data*9.12/58.6
    hdu.writeto("500mic_renormalized_to_1mm.fits")
    
datas, stds, hd, nhits = open_fits()
# norm, m2d, av, nhits_total = averaging(datas, stds, nhits)
averaging12(datas[0], datas[1], stds[0], stds[1], nhits[0], nhits[1])
# write_fits(hd, norm, m2d, av, nhits_total)
# compare_fits(datas)
# renormalized()

