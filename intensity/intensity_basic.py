##############################################
# Extracting intensity profile along a track #
##############################################

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import matplotlib.style as style
from astropy.io import fits
from astropy.convolution import convolve, Gaussian1DKernel, Box1DKernel
from scipy.ndimage import gaussian_filter

import sys
sys.path.insert(0, '..')
from constants import *

style.use('seaborn-poster') #sets the size of the charts
style.use('ggplot')

kernelG = 3
beam = 1.13*(17*sec)**2
cdelt = 0.7260188014754e-03

def load_fits(name):
    with fits.open(name) as hdu:
        return hdu[0].data
    # All the fits has been processed to conform to this opening

def load_fits_n(name, n):
    # Open the nth image in fits
    with fits.open(name) as hdu:
        return hdu[n].data

def locate_max(data):
    return np.unravel_index(data.argmax(), data.shape)

def disc_mask(a, b, r, n, m):
    if r== 0:
        return np.zeros((n,m))
    y,x = np.ogrid[-a:n-a, -b:m-b]
    mask = x*x + y*y <= r*r
    A = np.ones((n, m))
    A[mask] = 0                                                                              
    return 1-A # returns a matrix with one INSIDE the circle and 0 OUTSIDE

def ring_mask(a, b, rmin, rmax, n, m):
    big_disc = disc_mask(a, b, rmax, n, m)
    small_disc = disc_mask(a, b, rmin, n, m)
    return big_disc - small_disc

if __name__ == '__main__':
    l1521e_250mic  = load_fits('../fits_l1521e/l1521e_13mm.fits')
    ind = np.where(np.isnan(l1521e_250mic))
    l1521e_250mic[ind] = 0
    l1521e_250mic = gaussian_filter(l1521e_250mic, 5)
    xmax, ymax = locate_max(l1521e_250mic)
    print(xmax, ymax)
    uo = open('rprofile.dat', 'w')
    for i in range(25):
        uo.write("%f %.3f\n"%(0.1e-02*i*3600*np.sqrt(2),l1521e_250mic[xmax+i, ymax+i]))
        l1521e_250mic[xmax+i, ymax+i] = 0
    uo.close()
    plt.imshow(l1521e_250mic)
    plt.colorbar()
    plt.show()
