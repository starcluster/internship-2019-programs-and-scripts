##########################################
# Extracting intensity from linear track #
##########################################

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.style as style
from astropy.io import fits
from astropy.convolution import convolve, Gaussian1DKernel, Box1DKernel
from scipy.ndimage import gaussian_filter
from intensity import load_fits, display, locate_max, disc_mask, ring_mask, aperture_in_pixel, pixel_in_aperture_arcsec

style.use('seaborn-poster') #sets the size of the charts
style.use('ggplot')

import sys
sys.path.insert(0, '..')
from constants import *

kernelG = 3

if __name__ == '__main__':
    f250, f350, f500,f1250, run1, run2, run3  = load_fits('data/250mic.fits'),load_fits('data/350mic.fits'), load_fits('data/500mic.fits'), load_fits('data/1250mic_flatten.fits'), load_fits('data/run1.fits'), load_fits('data/run2.fits'), load_fits('data/run3.fits')

    beam = 1.13*(17*sec)**2

    run1 = 1/beam*run1*1e-6
    run2 = 1/beam*run2*1e-6
    run3 = 1/beam*run3*1e-6
    
    run1G = gaussian_filter(run1, kernelG)
    run2G = gaussian_filter(run2, kernelG)
    run3G = gaussian_filter(run3, kernelG)

    xmax, ymax = locate_max(f1250)
    print(xmax, ymax)
    print(f1250[xmax, ymax])

    #plt.imshow(f1250, origin='upper')
    a = ymax/xmax
    intensity = []
    R = []
    N = 20
    
    for i in range(20):
        xi = int(ymax + i*a*ymax/N/2)
        yi = int(xmax - i*a*xmax/N/2)
        #plt.plot(xi, yi , marker='*', color='white')
        masked = f1250*disc_mask(yi, xi, 6, 172, 165)
        ind = (masked != 0)
        intensity.append(np.average(masked[ind]))
        R.append(np.sqrt((a*i)**2 + (a*i)**2))
    plt.show()
    intensity = np.array(intensity)
    plt.title('1250µ')
    plt.xlabel("Distance to peak (pixels)")
    plt.ylabel("Intensity (MJy/sr)")
    plt.plot(R, intensity, '.')
    plt.show()

    
    
