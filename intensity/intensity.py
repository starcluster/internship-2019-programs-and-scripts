#############################################
# Extracting intensity from concentric ring #
#############################################

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import matplotlib.style as style
from astropy.io import fits
from astropy.convolution import convolve, Gaussian1DKernel, Box1DKernel
from scipy.ndimage import gaussian_filter

import sys
sys.path.insert(0, '..')
from constants import *

style.use('seaborn-poster') #sets the size of the charts
style.use('ggplot')

kernelG = 3
beam = 1.13*(17*sec)**2
cdelt = 0.7260188014754e-03

def load_fits(name):
    with fits.open(name) as hdu:
        return hdu[0].data
    # All the fits has been processed to conform to this opening

def load_fits_n(name, n):
    # Open the nth image in fits
    with fits.open(name) as hdu:
        return hdu[n].data

def display(f1250, run1, run2, run3):
    i = 1
    for img in [f1250, run1, run2, run3]:
        plt.subplot(2,2,i)
        plt.imshow(img)
        i += 1
    plt.show()

def locate_max(data):
    return np.unravel_index(data.argmax(), data.shape)

def disc_mask(a, b, r, n, m):
    if r== 0:
        return np.zeros((n,m))
    y,x = np.ogrid[-a:n-a, -b:m-b]
    mask = x*x + y*y <= r*r                                                                       
    A = np.ones((n, m))                                                                           
    A[mask] = 0                                                                                   
    return 1-A # returns a matrix with one INSIDE the circle and 0 OUTSIDE

def ring_mask(a, b, rmin, rmax, n, m):
    big_disc = disc_mask(a, b, rmax, n, m)
    small_disc = disc_mask(a, b, rmin, n, m)
    return big_disc - small_disc

def data_1mm():
    run1mm = load_fits_n('data/average_1mm.fits', 1)
    run1mm = 1/beam*run1mm*1e-6
    run1mmG = gaussian_filter(run1mm, kernelG*2)
    xmax, ymax = locate_max(run1mmG)
    print(xmax, ymax)
    mask = disc_mask(xmax, ymax, aperture_in_pixel(cdelt, 20), 451, 451)
    ind = (mask != 0)
    print(np.average(run1mm[ind]))
    plt.imshow(run1mm*mask)
    plt.show()

if __name__ == '__main__':
    l1521e_250mic , f250, f350, f500,f1250, run1, run2, run3, avg1mm, avg2mm  = load_fits('../fits_l1521e/l1521e_250mic.fits'),load_fits('../fits/250mic.fits'),load_fits('../fits/350mic.fits'), load_fits('../fits/500mic.fits'), load_fits('data/1250mic_flatten.fits'), load_fits('../fits/run1.fits'), load_fits('../fits/run2.fits'), load_fits('../fits/run3.fits'), load_fits('../fits/1mm_12.fits'), load_fits('../fits/2mm_12.fits')

    run1 = 1/beam*run1*1e-6
    run2 = 1/beam*run2*1e-6
    run3 = 1/beam*run3*1e-6
    
    beam = 1.13*(12.5*sec)**2
    avg1mm *= 1/beam*1e-6

    run1G = gaussian_filter(run1, kernelG)
    run2G = gaussian_filter(run2, kernelG)
    run3G = gaussian_filter(run3, kernelG)

    display(f1250, run1G, run2G, run3G)
    data_1mm()
    #exit()
    xmax, ymax = 0, 0

    for d in [f1250, run1G, run2G, run3G]:
        xmax, ymax = xmax + locate_max(d)[0],ymax + locate_max(d)[1]
        print(locate_max(d))
    xmax, ymax = int(xmax/4), int(ymax/4)
    plt.imshow(l1521e_250mic)
    plt.show()
    xmax, ymax = locate_max(gaussian_filter(l1521e_250mic, 5))
    print(xmax, ymax)
    print(run1G[xmax, ymax])
    R0 = aperture_in_pixel(cdelt,3)
    mat = np.zeros_like(l1521e_250mic)
    ind = np.where(np.isnan(l1521e_250mic))
    l1521e_250mic[ind] = 0
    xmax, ymax = locate_max(gaussian_filter(l1521e_250mic, 5))
    x = []
    y = [],[],[],[],[],[],[]
    uo = open('rprofile.dat','w')
    Rmax =  0.
    #for i in range(60):
    i = 0
    while Rmax < 15:
        Rmin = Rmax
        Rmax =  R0*np.sqrt(i+1)
        # pixel_in_aperture_arcsec(cdelt, Rmax/3600)
        x.append(Rmax*cdelt*3600)
        #plt.subplot(2,4,i+1)

        npix = np.sum(mat)
        uo.write("%10.2f%10.2f%6d" % (Rmin,Rmax,npix))
        j = 0
        for d in [l1521e_250mic]:# [f250, f350, f500, f1250,avg1mm,avg2mm]:
            n, m = d.shape
            print(xmax, ymax, Rmin, Rmax, n, m)
            mat = ring_mask(xmax, ymax, Rmin, Rmax, n, m)
            masked = mat*d
            ind = (masked !=0)
            avg = np.average(masked[ind])
            y[j].append(avg)
            sigma = np.std(masked[ind])
            # print("avg, sigma",j,avg, sigma)
            uo.write("%12.3e" % (avg))
            j += 1
        uo.write("\n")
        i += 1
    uo.close()
    exit()
    names = ['250', '350', '500', '1250', '2000run1', '2000run2', '2000run3']
    x = np.array(x)/cdelt/3600
    for k in range(7):
        plt.loglog(x, y[k]/np.max(y[k]), '.', label=names[k])
            #plt.loglog(x_aperture, y_intensity)
    plt.xlabel("Distance to the peak - Arcsec")
    plt.ylabel("Intensity - normalized")
    plt.legend()
    plt.show()
    uo = open('run2.dat', 'w')
    ns = 0
    for i in range(len(x)):
        uo.write("%.4f %.4f %.4f %.4f\n"%(x[i], y[ns][i], y[ns][i]/np.max(y[ns]), np.std(y[ns]/np.max(y[ns]))))
    uo.close()
    plt.loglog(x,y[ns]/np.max(y[ns]), '.')
    plt.show()
    run12 = load_fits_n("run12.fits", 1)
    run12 = run12*1/beam*1e-6
    run12 = gaussian_filter(run12, 10)
    plt.imshow(run12)
    plt.show()
    a = locate_max(run12)
    mask = ring_mask(a[0], a[1], 0, 20, 451, 451)
    masked = run12*mask
    ind = (mask != 0)
    print(np.average(masked[ind]), np.std(masked[ind]))
    
               
    #plt.imshow(masked)
    #plt.text(5,30,str(np.round(avg,3)) + '\n '+ str(np.round(sigma,2)),  bbox=dict(facecolor='white', alpha=0.5))
    # plt.text(5,30,r"""$\mu$=%.2f
# $\sigma$=%.2f
# $R$=%.2f """%(avg,sigma,Rmax),  bbox=dict(facecolor='white', alpha=0.5))
    # plt.title('Number of pixel = ' + str(np.sum(mat)))
# plt.show()

