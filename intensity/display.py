import numpy as np
import matplotlib.pyplot as plt
from scipy.ndimage import gaussian_filter
from intensity import *

kernelG11 = aperture_in_pixel(cdelt, 12.5)
kernelG17 = aperture_in_pixel(cdelt, 18.5)

if __name__ == '__main__':
    for i in range(1,4):
        plt.subplot(2,3,i)
        plt.imshow(gaussian_filter(load_fits_n('data/raw_map' + str(i) + '.fits', 1), kernelG11))
#        plt.imshow(load_fits_n('data/raw_map' + str(i) + '.fits', 1))
        plt.colorbar()
        plt.clim(-0.01,0.01)
        plt.title("Brightness 1mm run %s"%str(i))
    for i in range(1,4):
        plt.subplot(2,3,i+3)
        plt.imshow(gaussian_filter(load_fits_n('data/raw_map' + str(i) + '.fits', 4), kernelG17))
#        plt.imshow(load_fits_n('data/raw_map' + str(i) + '.fits', 4))
        plt.colorbar()
        plt.clim(-0.002,0.002)
        plt.title("Brightness 2mm run %s"%str(i))
    plt.suptitle("convolved by 12.5'' and 18.5'' gaussian")
    plt.show()
