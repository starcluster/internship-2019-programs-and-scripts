########################################################
# Extracting intensity from concentric elipsoids rings #
########################################################

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.style as style
from astropy.io import fits
from astropy.convolution import convolve, Gaussian1DKernel, Box1DKernel
from scipy.ndimage import gaussian_filter
from intensity import load_fits, display, locate_max, disc_mask, ring_mask, aperture_in_pixel, pixel_in_aperture_arcsec

import sys
sys.path.insert(0, '..')
from constants import *

style.use('seaborn-poster') #sets the size of the charts
style.use('ggplot')

kernelG = 3

def elipse_mask(a, b, rmin, rmax, n, m, theta=0):
    if rmin == 0 or rmax == 0:
        return np.zeros((n,m))
    y,x = np.ogrid[-a:n-a, -b:m-b]
    mask = x**2*((np.cos(theta)/rmin)**2 + (np.sin(theta)/rmax)**2) + y**2*((np.sin(theta)/rmin)**2 + (np.cos(theta)/rmax)**2) + 2*x*y*np.cos(theta)*np.sin(theta)*(1/rmin**2 - 1/rmax**2) <= 1
    A = np.ones((n, m))                                                                           
    A[mask] = 0                                                                                   
    return 1-A # returns a matrix with one INSIDE the circle and 0 OUTSIDE

def elipse_ring(a, b, rmin_out, rmax_out, rmin_in, rmax_in, n, m, theta):
    return elipse_mask(a, b, rmin_out, rmax_out, n, m, theta) - elipse_mask(a, b, rmin_in, rmax_in, n, m, theta)

def rotate(x, theta):
    rot = np.array([[np.cos(theta), np.sin(theta)],[-np.sin(theta), np.cos(theta)]])
    return np.dot(rot,x)

# plt.imshow(elipse_ring(100, 100, 10, 20, 5, 10, 200, 200, np.pi/3))
# plt.show()

if __name__ == '__main__':
    f250, f350, f500,f1250, run1, run2, run3  = load_fits('data/250mic.fits'),load_fits('data/350mic.fits'), load_fits('data/500mic.fits'), load_fits('data/1250mic_flatten.fits'), load_fits('data/run1.fits'), load_fits('data/run2.fits'), load_fits('data/run3.fits')

    beam = 1.13*(17*sec)**2

    run1 = 1/beam*run1*1e-6
    run2 = 1/beam*run2*1e-6
    run3 = 1/beam*run3*1e-6
    
    run1G = gaussian_filter(run1, kernelG)
    run2G = gaussian_filter(run2, kernelG)
    run3G = gaussian_filter(run3, kernelG)

    # display(f1250, run1G, run2G, run3G)
    
    xmax, ymax = 0, 0

    for d in [f1250, run1G, run2G, run3G]:
        xmax, ymax = xmax + locate_max(d)[0],ymax + locate_max(d)[1]
        print(locate_max(d))
    xmax, ymax = int(xmax/4), int(ymax/4)
    xmax, ymax = locate_max(f1250)
    print(xmax, ymax)
    print(run1G[xmax, ymax])
    cdelt = 0.7260188014754e-03
    R0_NS = aperture_in_pixel(cdelt,12)
    R0_EO = 0.6*R0_NS
    mat = np.zeros((165,172))
    # mat = elipse_ring(xmax, ymax, R0, R0*2, 0, 0, 172, 165, np.pi/3)
    # plt.imshow(mat*f1250)
    # plt.show()
    x = []
    y = [],[],[],[],[],[],[]
    uo = open('rprofile.dat','w')
    R_NS_max =  0
    R_EO_max = 0
    R_NS_min =  0
    R_EO_min = 0
    i = 0
    xmax, ymax = 85, 80
    while R_EO_max < 30:
        R_NS_min = R_NS_max
        R_EO_min = R_EO_max
        R_NS_max = R0_NS*np.sqrt(i+1)
        R_EO_max = R0_EO*np.sqrt(i+1)
        x.append(R_EO_max)
        #plt.subplot(2,4,i+1)
        mat = elipse_ring(xmax, ymax, R_NS_max, R_EO_max, R_NS_min, R_EO_min, 172, 165,  40*2*np.pi/360)
        #plt.imshow(mat*f1250)
        #plt.show()
        npix = np.sum(mat)
        # uo.write("%10.2f%10.2f%6d" % (Rmin,Rmax,npix))
        j = 0
        for d in [f250, f350, f500, f1250,run1G,run2G,run3G]:
            masked = mat*d
            ind = (masked !=0)
            avg = np.average(masked[ind])
            y[j].append(avg)
            sigma = np.std(masked[ind])
            uo.write("%12.3e" % (avg))
            j += 1
        uo.write("\n")
        i += 1
    uo.close()
    names = ['250', '350', '500', '1250', '2000run1', '2000run2', '2000run3']
    x = np.array(x)/cdelt/3600
    for k in range(7):
        plt.loglog(x, y[k]/np.max(y[k]), '.', label=names[k])
    plt.xlabel("Distance to the peak - Arcsec")
    plt.ylabel("Intensity - normalized")
    plt.legend()
    plt.show()

