set terminal wxt size 1200,800 enhanced font 'Verdana,10' persist
set border linewidth 1.5
set style line 1 linecolor rgb '#FF0000' linetype 1 linewidth 2
set style line 2 linecolor rgb '#000080' linetype 1 linewidth 2
set samples 10000
set title "Intensity values along a track"
set xlabel "Distance to the peak ['']"
set ylabel "Intensity (normalized)"
set logscale

set terminal pngcairo size 1200,800
alpha = 2
x0 = 96
A=140
f(x) = A/(1+(x/x0)**alpha)
fit f(x) 'rprofile.dat' u 1:2 via alpha, x0, A
set label sprintf("alpha = %.3f",alpha) at 30,30
bfit = gprintf("x0 = %.3f",x0)
set label bfit at 30,20
show label
plot  'rprofile.dat' u 1:2, f(x)
