#############################################
# Extracting intensity from concentric ring #
#############################################

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.style as style
from astropy.io import fits
from astropy.convolution import convolve, Gaussian1DKernel, Box1DKernel
from scipy.ndimage import gaussian_filter

style.use('seaborn-poster') #sets the size of the charts
style.use('ggplot')

hp = 6.626e-34
kb = 1.38e-23
cc = 3e8
sec = 1/206265
mJytoMJy = 1e9

def load_fits(name):
    with fits.open(name) as hdu:
        tmp = hdu[0].data[:,0]
        tmp = tmp[0]
    ind = np.where(np.isnan(tmp))
    tmp[ind] = 0
    print(tmp)
    hdu2 = fits.PrimaryHDU(tmp)
    hdu2.writeto('1250mic_flatten.fits')

load_fits('1250mic.fits')
    
