set terminal wxt size 1200,800 enhanced font 'Verdana,10' persist
# set terminal latex
set border linewidth 1.5
set style line 1 linecolor rgb '#FF0000' linetype 1 linewidth 2
set style line 2 linecolor rgb '#000080' linetype 1 linewidth 2
set samples 10000
set title "Intensity calculated with concentric ring"
set xlabel "Distance to the peak ['']"
set ylabel "Intensity (normalized)"
set logscale
# set terminal pngcairo size 1200,800
alpha = 2
A = 1
x0 = 20
f(x) = A/(1+(x/x0)**alpha)
fit f(x) "run2.dat" u 1:3 via alpha, x0
plot "run2.dat" u 1:3 title "Intensity on run 2", f(x)
